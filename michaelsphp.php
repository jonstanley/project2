<?php
	session_start();
	
	if(isset($_SESSION['userID']) || isset($_POST['userID'])){
		if(isset($_POST['userID'])){
			$userID = $_POST['userID'];
		
			if($userID > 1000 && $userID < 1050){
				print "
					Valid ID detected.<br />
				";
				
				$_SESSION['userID'] = $userID;
			}
		
			else{
				print "
					Invalid ID input. <br />
					<a href = 'index.php' >Retry</a>
				";
			}
		}
		
		if(isset($_SESSION['userID'])){
			$userID = $_SESSION['userID'];
		
			include_once "connection.php";
			mysql_select_db("mi664555", $connection);

			$query = "SELECT * FROM workshop_p2 WHERE user_id = '$userID'";
			$result = mysql_query($query);
			$count = mysql_fetch_array($result);
				
			if(isset($_POST['candidate'])){
				$candidate = $_POST['candidate'];
				
				$insert = "INSERT INTO workshop_p2 (user_id, candidate) VALUES ('$userID', '$candidate');";	
				mysql_query($insert);
				
				print "
					You voted for candidate $candidate </br>
					<a href = 'index.php' >Home</a>
				";
			}
				
			else if($count == ""){
				print "
					You have not voted yet! <br />
					
					<form action = 'index.php' method = 'post'>
						<input type = 'submit' name = 'candidate' value = '1'/>
						<input type = 'submit' name = 'candidate' value = '2' />
						<input type = 'submit' name = 'candidate' value = '3' />
					</form>
				";

			}
			
			else{
				$result = mysql_query($query);
				while($data = mysql_fetch_array($result)){
					$user_id = $data['user_id'];
					$candidate = $data['candidate'];
					
					print "
						You, user #$user_id, voted for candidate #$candidate. <br />
					";
				}
				
				$query = "SELECT candidate FROM workshop_p2";
				$result = mysql_query($query);
				
				//Vote Counter
				$candidate_1 = 0;
				$candidate_2 = 0;
				$candidate_3 = 0;
						
				while($data = mysql_fetch_array($result)){
					$candidate = $data['candidate'];
					
					if($candidate == "1"){
						$candidate_1++;
					}
					else if($candidate == "2"){
						$candidate_2++;
					}
					else if($candidate == "3"){
						$candidate_3++;
					}
				}
				
				//Percent Calculator
				$total = $candidate_1 + $candidate_2 + $candidate_3;
				$percent_1 = substr(($candidate_1/$total)*100, 0, 5);
				$percent_2 = substr(($candidate_2/$total)*100, 0, 5);
				$percent_3 = substr(($candidate_3/$total)*100, 0, 5);
				
				$round_1 = round($percent_1)*5;
				$round_2 = round($percent_2)*5;
				//$round_3 = round($percent_3)*5;
				$round_3 = 500 - ($round_1 + $round_2);
				
				print "
					<hr />
					
					<h3>Style #1</h3>
					Candidate 1: $percent_1% ($candidate_1 votes)<br />
					Candidate 2: $percent_2% ($candidate_2 votes)<br />
					Candidate 3: $percent_3% ($candidate_3 votes)<br />
					Total: $total votes <br />
					
					<h3>Style #2</h3>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px;'>
						<div class = 'candidate_1' style = 'width: ".$round_1."px; height: 25px; float: left; background-color: #ff0000;'>$percent_1% ($candidate_1)</div>
						<div class = 'candidate_2' style = 'width: ".$round_2."px; height: 25px; float: left; background-color: #00ff00;'>$percent_2% ($candidate_2)</div>
						<div class = 'candidate_3' style = 'width: ".$round_3."px; height: 25px; float: left; background-color: #0000ff;'>$percent_3% ($candidate_3)</div>
					</div>
					
					<h3>Style #3</h3>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px; margin-bottom: 5px;'>
						<div class = 'candidate_1' style = 'width: ".$round_1."px; height: 25px; float: left; background-color: #ff0000;'>#1 - Nicole Shebroe</div>
						$percent_1% ($candidate_1)
					</div>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px; margin-bottom: 5px;'>
						<div class = 'candidate_2' style = 'width: ".$round_2."px; height: 25px; float: left; background-color: #00ff00;'>#2 - Austin Munro</div>
						$percent_2% ($candidate_2)
					</div>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px;'>
						<div class = 'candidate_3' style = 'width: ".$round_3."px; height: 25px; float: left; background-color: #0000ff;'>#3 - Michael Ku</div>
						$percent_3% ($candidate_3)
					</div>
				";
			}
			mysql_close($connection);
		}
	}
	
	else{
		print "
			<form action = 'index.php' method = 'post'>
				<input type = 'text' name = 'userID' />
				<input type = 'submit' value = 'Register'>
			</form>
			<h3>Used IDs (1001-1049)</h3>
		";
		
		include_once "connection.php";
		mysql_select_db("mi664555", $connection);

		$query = "SELECT user_id FROM workshop_p2 ORDER BY user_id ASC";
		$result = mysql_query($query);
			
		while($data = mysql_fetch_array($result)){
			$id = $data['user_id'];
			print"$id<br />";
		}	
	}
?>