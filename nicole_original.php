<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Toon Town Voting</title> 
	
<meta name="viewport" content="width=device-width, initial-scale=1"> 
        
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0-alpha.1/jquery.mobile-1.2.0-alpha.1.min.css" />
 
     <link rel="stylesheet" href="themes/toons.css" />
	 <link rel="stylesheet" href="themes/styles.css" />
	 
	<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.2.0-alpha.1/jquery.mobile-1.2.0-alpha.1.min.js"></script>
    
	 <script type="text/javascript
		/*
		//Disabled Swipe because we probably don't want people jumping around.
		
		 $('div.ui-page').live("swipeleft", function(){
			var nextpage = $(this).next('div[data-role="page"]');
			// swipe using id of next page if exists
			if (nextpage.length > 0) {
				$.mobile.changePage(nextpage, {transition:"slide", reverse:false});
			}
		});
		
		$('div.ui-page').live("swiperight", function(){
			var prevpage = $(this).prev('div[data-role="page"]');
			// swipe using id of next page if exists
			if (prevpage.length > 0) {
				$.mobile.changePage(prevpage, {transition:"slide", reverse:true});
			}
		});
		*/
	 </script>
</head>

<body>

<?php	
	if(isset($_SESSION['userID']) || isset($_POST['userID'])){
		if(isset($_POST['userID'])){
			$userID = $_POST['userID'];
		
			if($userID > 1000 && $userID < 1050){
				print "
					Valid ID detected.<br />
				";
				
				$_SESSION['userID'] = $userID;
			}
		
			else{
				print "
					Invalid ID input. <br />
					<a href = 'index.php' >Retry</a>
				";
			}
		}
		
		if(isset($_SESSION['userID'])){
			$userID = $_SESSION['userID'];
		
			include_once "connection.php";
			mysql_select_db("mi664555", $connection);

			$query = "SELECT * FROM workshop_p2 WHERE user_id = '$userID'";
			$result = mysql_query($query);
			$count = mysql_fetch_array($result);
				
			if(isset($_POST['candidate'])){
				$candidate = $_POST['candidate'];
				
				$insert = "INSERT INTO workshop_p2 (user_id, candidate) VALUES ('$userID', '$candidate');";	
				mysql_query($insert);
				
				print "
					You voted for candidate $candidate </br>
					<a href = 'index.php' >Home</a>
				";
			}
				
			else if($count == ""){
				print "
					You have not voted yet! <br />
					
					<form action = 'index.php' method = 'post'>
						<input type = 'submit' name = 'candidate' value = '1'/>
						<input type = 'submit' name = 'candidate' value = '2' />
						<input type = 'submit' name = 'candidate' value = '3' />
					</form>
				";

			}
			
			else{
				$result = mysql_query($query);
				while($data = mysql_fetch_array($result)){
					$user_id = $data['user_id'];
					$candidate = $data['candidate'];
					
					print "
						You, user #$user_id, voted for candidate #$candidate. <br />
					";
				}
				
				$query = "SELECT candidate FROM workshop_p2";
				$result = mysql_query($query);
				
				//Vote Counter
				$candidate_1 = 0;
				$candidate_2 = 0;
				$candidate_3 = 0;
						
				while($data = mysql_fetch_array($result)){
					$candidate = $data['candidate'];
					
					if($candidate == "1"){
						$candidate_1++;
					}
					else if($candidate == "2"){
						$candidate_2++;
					}
					else if($candidate == "3"){
						$candidate_3++;
					}
				}
				
				//Percent Calculator
				$total = $candidate_1 + $candidate_2 + $candidate_3;
				$percent_1 = substr(($candidate_1/$total)*100, 0, 5);
				$percent_2 = substr(($candidate_2/$total)*100, 0, 5);
				$percent_3 = substr(($candidate_3/$total)*100, 0, 5);
				
				$round_1 = round($percent_1)*5;
				$round_2 = round($percent_2)*5;
				//$round_3 = round($percent_3)*5;
				$round_3 = 500 - ($round_1 + $round_2);
				
				print "
					<hr />
					
					<h3>Style #1</h3>
					Candidate 1: $percent_1% ($candidate_1 votes)<br />
					Candidate 2: $percent_2% ($candidate_2 votes)<br />
					Candidate 3: $percent_3% ($candidate_3 votes)<br />
					Total: $total votes <br />
					
					<h3>Style #2</h3>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px;'>
						<div class = 'candidate_1' style = 'width: ".$round_1."px; height: 25px; float: left; background-color: #ff0000;'>$percent_1% ($candidate_1)</div>
						<div class = 'candidate_2' style = 'width: ".$round_2."px; height: 25px; float: left; background-color: #00ff00;'>$percent_2% ($candidate_2)</div>
						<div class = 'candidate_3' style = 'width: ".$round_3."px; height: 25px; float: left; background-color: #0000ff;'>$percent_3% ($candidate_3)</div>
					</div>
					
					<h3>Style #3</h3>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px; margin-bottom: 5px;'>
						<div class = 'candidate_1' style = 'width: ".$round_1."px; height: 25px; float: left; background-color: #ff0000;'>#1 - Nicole Shebroe</div>
						$percent_1% ($candidate_1)
					</div>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px; margin-bottom: 5px;'>
						<div class = 'candidate_2' style = 'width: ".$round_2."px; height: 25px; float: left; background-color: #00ff00;'>#2 - Austin Munro</div>
						$percent_2% ($candidate_2)
					</div>
					<div class = 'bar' style = 'width: 500px; height: 25px; border: solid black 1px;'>
						<div class = 'candidate_3' style = 'width: ".$round_3."px; height: 25px; float: left; background-color: #0000ff;'>#3 - Michael Ku</div>
						$percent_3% ($candidate_3)
					</div>
				";
			}
			mysql_close($connection);
		}
	}
	
	else{
		//header('Location: #login');
	}
?>




	<!-- Login -->
	<div data-role="page" id="login">

		<div data-role="header">
			<center>
		<a href=""><img src="img/baby.png"/></a>
		</center>
		</div><!-- /header -->

		<div data-role="content">
			<div data-role="fieldcontain">
				<form action = 'index.php' method = 'post'>
				<input type = 'text' name = 'userID' placeholder = "User ID"/>
				<input type = 'submit' value = 'Register'>
			</div>
		</div><!-- /content -->
	</div>


	<!-- /page -->


	<!-- Results -->
	<div data-role="page" id="results">

		<div data-role="header">
			<center>
		<a href=""><img src="img/baby.png"/></a>
		</center>
		</div><!-- /header -->

		<div data-role="content">	
			<div class="candidate">
				<h1>Baby Bugs Bunny</h1>
				<div class="candpic">
					<img src="img/candidate1.gif" alt="candidate1" />
					<p>Baby Bugs is a stand-up kind of guy. He will make a difference!</p>
				</div>
				<button id = 'vote1'>Vote for Baby Bugs</button>	
			</div>
		</div><!-- /content -->
	</div><!-- /page -->



	<!-- Candidates -->
	<div data-role="page" id="candidates">

		<div data-role="header">
			<center>
		<a href=""><img src="img/baby.png"/></a>
		</center>
		</div><!-- /header -->

		<div data-role="content">	
			<div class="candidate">
				<h1>Meet the Candidates</h1>
				<div class="candpic">
					<p>Here is who you can vote for:</p>
					<a href="#candidate1" data-role="button" data-inline='true'> View Baby Bugs</a>
					<a href="#candidate2" data-role="button" data-inline='true'> View Baby Daffy</a>
					<a href="#candidate3" data-role="button" data-inline='true'> View Baby Taz</a>
				</div>	
			</div>
		</div><!-- /content -->
	</div><!-- /page -->


	<!-- Candidate 1 -->
	<div data-role="page" id="candidate1">

		<div data-role="header">
			<center>
		<a href=""><img src="img/baby.png"/></a>
		</center>
		</div><!-- /header -->

		<div data-role="content">	
			<div class="candidate">
				<h1>Baby Bugs Bunny</h1>
				<div class="candpic">
					<img src="img/candidate1.gif" alt="candidate1" />
					<p>Baby Bugs is a stand-up kind of guy. He will make a difference!</p>
				</div>
				<button id = 'vote1'>Vote for Baby Bugs</button>
				<a href="#candidates" data-role="button" data-inline='true'>Back to Candidates</a>
				<!-- MKU: Put in individual total of votes for each candidate after vote is submitted--!>	
			</div>
		</div><!-- /content -->
	</div><!-- /page -->



	<!-- Candidate 2 -->
	<div data-role="page" id="candidate2">

		<div data-role="header">
			<center>
		<a href=""><img src="img/baby.png"/></a>
		</center>
		</div><!-- /header -->

		<div data-role="content">	
			<div class="candidate">
				<h1>Baby Daffy Duck</h1>
				<div class="candpic">
					<img src="img/candidate2.gif" alt="candidate2" />
					<p>Baby Daffy wants only the best. He will quack his way to the top.</p>
				</div>
				<button id = 'vote1'>Vote for Baby Daffy</button>
				<a href="#candidates" data-role="button" data-inline='true'>Back to Candidates</a>
				<!-- MKU: Put in individual total of votes for each candidate after vote is submitted--!>
			</div>
		</div><!-- /content -->
	</div><!-- /page -->


	<!-- Candidate 3 -->
	<div data-role="page" id="candidate3">

		<div data-role="header">
			<center>
		<a href=""><img src="img/baby.png"/></a>
		</center>
		</div><!-- /header -->

		<div data-role="content">	
			<div class="candidate">
				<h1>Baby Tazmanian Devil</h1>
				<div class="candpic">
					<img src="img/candidate3.jpg" alt="candidate3" />
					<p>Baby Taz won't mess around. He's in it for the fun of it!</p>
				</div>
				<button id = 'vote1'>Vote for Baby Taz</button>
				<a href="#candidates" data-role="button" data-inline='true'>Back to Candidates</a>
				<!-- MKU: Put in individual total of votes for each candidate after vote is submitted--!>
			</div>
		</div><!-- /content -->
	</div><!-- /page -->
</body>
</html>