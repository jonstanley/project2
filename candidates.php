<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Toon Town Voting</title> 
	
<meta name="viewport" content="width=device-width, initial-scale=1"> 
        
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0-alpha.1/jquery.mobile-1.2.0-alpha.1.min.css" />
 
     <link rel="stylesheet" href="themes/toons.css" />
	 <link rel="stylesheet" href="themes/styles.css" />
	 
	<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.2.0-alpha.1/jquery.mobile-1.2.0-alpha.1.min.js"></script>
    
</head>
<body>
	<?php	
		if(isset($_SESSION['userID'])){
			$userID = $_SESSION['userID'];
		
			include_once "connection.php";
			mysql_select_db("mi664555", $connection);

			$query = "SELECT * FROM workshop_p2 WHERE user_id = '$userID'";
			$result = mysql_query($query);
			$count = mysql_fetch_array($result);
									
			if($count == ""){
				?>
					<!-- Candidates -->
					<div data-role="page" id="candidates">

						<div data-role="header">
							<center>
						<a href=""><img src="img/baby.png"/></a>
						</center>
						</div><!-- /header -->

						<div data-role="content">	
							<div class="candidate">
								<h1>Meet the Candidates</h1>
								<div class="candpic">
									<p>Here is who you can vote for:</p>
									<a href="#candidate1" data-role="button" data-inline='true'> View Baby Bugs</a>
									<a href="#candidate2" data-role="button" data-inline='true'> View Baby Daffy</a>
									<a href="#candidate3" data-role="button" data-inline='true'> View Baby Taz</a>
								</div>	
							</div>
						</div><!-- /content -->
					</div><!-- /page -->
					
					<!-- Candidate 1 -->
					<div data-role="page" id="candidate1">

						<div data-role="header">
							<center>
						<a href=""><img src="img/baby.png"/></a>
						</center>
						</div><!-- /header -->

						<div data-role="content">	
							<div class="candidate">
								<h1>Baby Bugs Bunny</h1>
								<div class="candpic">
									<img src="img/candidate1.gif" alt="candidate1" />
									<p>Baby Bugs is a stand-up kind of guy. He will make a difference!</p>
								</div>
								<form action = 'vote.php' method = 'post'>
									<input type = 'submit' name = 'candidate' value = 'Vote for Baby Bugs'/></input>
								</form>
								<a href="#candidates" data-role="button" data-inline='true'>Back to Candidates</a>
								<!-- MKU: Put in individual total of votes for each candidate after vote is submitted--!>	
							</div>
						</div><!-- /content -->
					</div><!-- /page -->



					<!-- Candidate 2 -->
					<div data-role="page" id="candidate2">

						<div data-role="header">
							<center>
						<a href=""><img src="img/baby.png"/></a>
						</center>
						</div><!-- /header -->

						<div data-role="content">	
							<div class="candidate">
								<h1>Baby Daffy Duck</h1>
								<div class="candpic">
									<img src="img/candidate2.gif" alt="candidate2" />
									<p>Baby Daffy wants only the best. He will quack his way to the top.</p>
								</div>
								<form action = 'vote.php' method = 'post'>
									<input type = 'submit' name = 'candidate' value = 'Vote for Baby Daffy'/></input>
								</form>
								<a href="#candidates" data-role="button" data-inline='true'>Back to Candidates</a>
								<!-- MKU: Put in individual total of votes for each candidate after vote is submitted--!>
							</div>
						</div><!-- /content -->
					</div><!-- /page -->


					<!-- Candidate 3 -->
					<div data-role="page" id="candidate3">

						<div data-role="header">
							<center>
						<a href=""><img src="img/baby.png"/></a>
						</center>
						</div><!-- /header -->

						<div data-role="content">	
							<div class="candidate">
								<h1>Baby Tazmanian Devil</h1>
								<div class="candpic">
									<img src="img/candidate3.gif" alt="candidate3" />
									<p>Baby Taz won't mess around. He's in it for the fun of it!</p>
								</div>
								<form action = 'vote.php' method = 'post'>
									<input type = 'submit' name = 'candidate' value = 'Vote for Baby Taz'/></input>
								</form>
								<a href="#candidates" data-role="button" data-inline='true'>Back to Candidates</a>
								<!-- MKU: Put in individual total of votes for each candidate after vote is submitted--!>
							</div>
						</div><!-- /content -->
					</div><!-- /page -->
				<?php
			}
			
			else{
				header('Location: results.php');	
			}
			mysql_close($connection);
		}
		
		else{
			header('Location: index.php');
		}
	?>
</body>
</html>