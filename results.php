<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Toon Town Voting</title> 
	
<meta name="viewport" content="width=device-width, initial-scale=1"> 
        
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0-alpha.1/jquery.mobile-1.2.0-alpha.1.min.css" />
 
     <link rel="stylesheet" href="themes/toons.css" />
	 <link rel="stylesheet" href="themes/styles.css" />
	 
	<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.2.0-alpha.1/jquery.mobile-1.2.0-alpha.1.min.js"></script>
    
</head>
<body>
	<?php	
		if(isset($_SESSION['userID']) || isset($_POST['userID'])){
			if(isset($_POST['userID'])){
				$userID = $_POST['userID'];
			
				if($userID > 1000 && $userID < 1050){					
					$_SESSION['userID'] = $userID;
				}
			
				else{
					print "
						Invalid ID input. <br />
						<a href = 'index.php' >Retry</a>
					";
				}
			}
			
			if(isset($_SESSION['userID'])){
				$userID = $_SESSION['userID'];
		
				include_once "connection.php";
				mysql_select_db("mi664555", $connection);

				$query = "SELECT * FROM workshop_p2 WHERE user_id = '$userID'";
				$result = mysql_query($query);
				$count = mysql_fetch_array($result);
									
				if($count == ""){
					//MAKE THEM VOTE, MOTHERFUCKER
					header('Location: candidates.php');
				}
			
				else{
					$result = mysql_query($query);
					while($data = mysql_fetch_array($result)){
						$user_id = $data['user_id'];
						$candidate = $data['candidate'];
						
						print "
							You, user #$user_id, voted for candidate #$candidate. <br />
						";
					}
					
					$query = "SELECT candidate FROM workshop_p2";
					$result = mysql_query($query);
					
					//Vote Counter
					$candidate_1 = 0;
					$candidate_2 = 0;
					$candidate_3 = 0;
							
					while($data = mysql_fetch_array($result)){
						$candidates = $data['candidate'];
						
						if($candidates == "1"){
							$candidate_1++;
						}
						else if($candidates == "2"){
							$candidate_2++;
						}
						else if($candidates == "3"){
							$candidate_3++;
						}
					}
					
					//Percent Calculator
					$total = $candidate_1 + $candidate_2 + $candidate_3;
					$percent_1 = substr(($candidate_1/$total)*100, 0, 5);
					$percent_2 = substr(($candidate_2/$total)*100, 0, 5);
					$percent_3 = substr(($candidate_3/$total)*100, 0, 5);
					
					$round_1 = round($percent_1)*5;
					$round_2 = round($percent_2)*5;
					//$round_3 = round($percent_3)*5;
					$round_3 = 500 - ($round_1 + $round_2);
					
					if($candidate == 1){
						$votee = "Baby Bugs Bunny";
						$desc = "Baby Bugs is a stand-up kind of guy. He will make a difference!";
					}
					
					else if($candidate == 2){
						$votee = "Baby Daffy Duck";
						$desc = "Baby Daffy wants only the best. He will quack his way to the top.";
					}
					
					else if($candidate == 3){
						$votee = "Baby Tazmanian Devil";
						$desc = "Baby Taz won't mess around. He's in it for the fun of it!";
					}
					
					?>
						<!-- Results -->
						<div data-role="page" id="results">

							<div data-role="header">
								<center>
							<a href=""><img src="img/baby.png"/></a>
							</center>
							</div><!-- /header -->

							<div data-role="content">	
								<div class="candidate">
									<h2>You Voted For...</h2>
									<h1><?php echo $votee; ?></h1>
									<div class="candpic">
										<img src="img/candidate<?php echo $candidate; ?>.gif" alt="candidate<?php echo $candidate; ?>" />
										<p><?php echo $desc; ?></p>
									</div>
									
									<p>
										Check out who's winning the polls!<br /><br />
										#1 - Baby Bugs <?php print "$percent_1% ($candidate_1)"; ?><br />
										#2 - Baby Daffy <?php print "$percent_2% ($candidate_2)"; ?><br />
										#3 - Baby Taz <?php print "$percent_3% ($candidate_3)"; ?>
									</p>
									
								</div>
							</div><!-- /content -->
						</div><!-- /page -->						
					<?php
				}
				mysql_close($connection);
			}
		}
		
		else{
			header('Location: index.php');
		}
	?>
</body>
</html>