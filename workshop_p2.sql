-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: sulley.cah.ucf.edu
-- Generation Time: Oct 13, 2012 at 02:58 PM
-- Server version: 5.5.24-0ubuntu0.12.04.1
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mi664555`
--

-- --------------------------------------------------------

--
-- Table structure for table `workshop_p2`
--

CREATE TABLE IF NOT EXISTS `workshop_p2` (
  `user_id` int(4) NOT NULL,
  `candidate` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workshop_p2`
--

INSERT INTO `workshop_p2` (`user_id`, `candidate`) VALUES
(1010, 3),
(1001, 1),
(1002, 1),
(1003, 2),
(1030, 3),
(1048, 3),
(1004, 1),
(1008, 1),
(1006, 1),
(1011, 1),
(1024, 1),
(1025, 1),
(1026, 1),
(1027, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
